<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('clients', function(Blueprint $table)
		{
			$table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('contact');
            $table->string('nif');
            $table->integer('state');
            $table->string('server');
            $table->string('dns');
            $table->string('user');
            $table->string('password');
            $table->text('obs');
            $table->string('url');
            $table->string('provider');
            $table->timestamp('date_register');
            $table->timestamp('date_expiration');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('clients');
	}

}
