@extends('app')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-md-offset-4">
				<div class="login-panel panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Please Sign In</h3>
					</div>
					<div class="panel-body">
						@if (count($errors) > 0)
							<div class="alert alert-danger">
								<strong>Whoops!</strong> There were some problems with your input.<br><br>
								<ul>
									@foreach ($errors->all() as $error)
										<li>{{ $error }}</li>
									@endforeach
								</ul>
							</div>
						@endif
						<form role="form" role="form" method="POST" action="{{ url('/auth/login') }}">
							<fieldset>

								<input type="hidden" name="_token" value="{{ csrf_token() }}">

								<div class="form-group">
									<input class="form-control" placeholder="E-mail" name="email" type="email" value="{{ old('email') }}" autofocus>
								</div>

								<div class="form-group">
									<input class="form-control" placeholder="Password" name="password" type="password" value="">
								</div>


								<button type="submit" class="btn btn-primary">Login</button>
								<a class="btn btn-link" href="{{ url('/password/email') }}">Forgot Your Password?</a>

							</fieldset>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
