<div class="form-group">
    {!! Form::label('name','Name',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name',null,['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('email','E-Mail Address',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email('email',null,['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('contact','Phone Contact',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('contact',null,['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('nif','NIF',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nif',null,['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('state','State',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::label('state1','Active',['class' => 'radio-inline']) !!}
        {!! ($stateOption == 1)?Form::radio('state',1,true,['id' => 'state1']):Form::radio('state',1,false,['id' => 'state1']) !!}

        {!! Form::label('state2','Inactive',['class' => 'radio-inline']) !!}
        {!! ($stateOption == 0)?Form::radio('state',0,true,['id' => 'state2']):Form::radio('state',0,false,['id' => 'state2']) !!}

    </div>
</div>