@extends('app')

@section('content')
	<div id="wrapper">

		<!-- Navigation -->
		@include('menu')

		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Clients</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading text-right">
                            <a href="{{ url('/clients/create') }}" class="btn btn-primary text-right">
                                <i class="fa fa-plus-square"></i>
                                Add new
                            </a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper table-responsive">
                                <table class="table table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Contact</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@if (isset($clients) AND count($clients) > 0)
                                    		@foreach ($clients as $client)
												<tr class="odd gradeX">
	                                            <td>{{$client->name}}</td>
	                                            <td>{{$client->email}}</td>
	                                            <td>{{$client->contact}}</td>
	                                            <td class="center">
                                                    @if ($client->state == 1)
                                                        <span class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Active">
                                                            <i class="fa fa-info"></i>
                                                        </span>
                                                    @else
                                                        <span class="btn btn-warning btn-circle" data-toggle="tooltip" data-placement="top" title="Inactive">
                                                            <i class="fa fa-info"></i>
                                                        </span>
                                                    @endif
                                                </td>
	                                            <td class="center tdOptions">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-gear"></i>  <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a href="{{ url('/clients/'.$client->id.'/edit') }}">
                                                                    <i class="fa fa-edit"></i>
                                                                    Edit
                                                                </a>
                                                            </li>
                                                            <li>
                                                                {!! Form::open(array('method' => 'DELETE','route' => ['clients.destroy',$client->id])) !!}

                                                                <button type="submit" class="fa fa-trash">
                                                                    Delete
                                                                </button>

                                                                {!! Form::close() !!}
                                                            </li>
                                                           <!-- <li>
                                                                {!! Form::model($client, ['method' => 'PATCH', 'action' => ['ClientsController@update', $client->id], 'class' => 'form-horizontal']) !!}
                                                                @if ($client->state == 1)
                                                                    <input type="hidden" name="state" value="0"/>
                                                                    <button type="submit" class="fa">
                                                                        Inactive
                                                                    </button>

                                                                @else
                                                                    <input type="hidden" name="state" value="1"/>
                                                                    <button type="submit" class="fa">
                                                                        Active
                                                                    </button>

                                                                @endif
                                                                {!! Form::close() !!}

                                                            </li>-->
                                                        </ul>
                                                    </div>
	                                            </td>                                            
	                                        </tr>
											@endforeach
										@else
											<tr class="" colspan="5">
                                             	<td>No results </td>
                                        	</tr>
                                    	@endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
               
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
       
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
@endsection