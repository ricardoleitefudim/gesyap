<div class="form-group">
    {!! Form::label('server','Server',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('server',null,['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('dns','DNS',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('dns',null,['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('user','User',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('user',null,['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('password','Password',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('password',null,['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    {!! Form::label('obs','Obs.',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('obs',null,['class' => 'form-control']) !!}
    </div>
</div>