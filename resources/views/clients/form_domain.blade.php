<div class="form-group">
    {!! Form::label('url','Url',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('url',null,['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('provider','Provider',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('provider',null,['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('date_register','Register Date',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('date_register',null,['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('date_expiration','Expiration Date',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('date_expiration',null,['class' => 'form-control']) !!}
    </div>
</div>