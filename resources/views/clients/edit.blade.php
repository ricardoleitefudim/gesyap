@extends('app')

@section('content')
    <div id="wrapper">

        <!-- Navigation -->
        @include('menu')

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Clients</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">

                    <div class="panel panel-default">
                        <div class="panel-heading text-left">
                            <a href="{{ url('/clients') }}" class="btn btn-primary btn-circle">
                                <i class="fa fa-arrow-left"></i>
                            </a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#info" data-toggle="tab">Basic Info</a>
                                </li>
                                <li>
                                    <a href="#hosting" data-toggle="tab">Hosting</a>
                                </li>
                                <li>
                                    <a href="#domain" data-toggle="tab">Domain</a>
                                </li>

                            </ul>

                            <!-- Tab panes -->
                            {!! Form::model($client, ['method' => 'PATCH', 'action' => ['ClientsController@update', $client->id], 'class' => 'form-horizontal']) !!}

                            <div class="tab-content">

                                <div class="tab-pane fade in active" id="info">
                                    @include ('clients.form',['stateOption' => $client->state])
                                </div>
                                <div class="tab-pane fade" id="hosting">
                                    @include ('clients.form_hosting',[])
                                </div>
                                <div class="tab-pane fade" id="domain">
                                    @include ('clients.form_domain',[])
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    {!! Form::submit('Edit client',['class' => 'btn btn-primary form.control']) !!}
                                </div>
                            </div>

                            {!! Form::close() !!}
                            @include ('errors.list')
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>

        </div>

        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
@endsection