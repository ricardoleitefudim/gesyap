@extends('app')

@section('content')
	<div id="wrapper">

		<!-- Navigation -->
		@include('menu')

		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Users</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">

                        <div class="panel-heading text-left">
                            <a href="{{ url('/users') }}" class="btn btn-link text-left">
                                back
                            </a>
                        </div>

                        <div class="panel-body">


                            {!! Form::open(['url' => 'users', 'class' => 'form-horizontal']) !!}
                                @include ('users.form',['submitButtonText' => 'Add User','stateOption' => 1,'profileOption' => '-100'])
                            {!! Form::close() !!}

                            @include ('errors.list')

               
                        </div>

                    </div>



                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
       
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
@endsection