<div class="form-group">
    {!! Form::label('name','Name',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name',null,['class' => 'form-control']) !!}
    </div>
</div>

<div class="form-group">
    {!! Form::label('email','E-Mail Address',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::email('email',null,['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('password','Password',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('password',['class' => 'form-control']) !!}
    </div>
</div>


<div class="form-group">
    {!! Form::label('state','State',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::label('state1','Active',['class' => 'radio-inline']) !!}
        {!! ($stateOption == 1)?Form::radio('state',1,true,['id' => 'state1']):Form::radio('state',1,false,['id' => 'state1']) !!}

        {!! Form::label('state2','Inactive',['class' => 'radio-inline']) !!}
        {!! ($stateOption == 0)?Form::radio('state',0,true,['id' => 'state2']):Form::radio('state',0,false,['id' => 'state2']) !!}

    </div>
</div>
<div class="form-group">
    {!! Form::label('profile','Profile',['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('profile',array('-100' => '-- Select option --','1' => 'Comercial','3' => 'Director Comercial',
                        '4' => 'Admin Yap', '5' => 'Administrador'),$profileOption,['class' => 'form-control']) !!}

    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        {!! Form::submit($submitButtonText,['class' => 'btn btn-primary form.control']) !!}
    </div>
</div>