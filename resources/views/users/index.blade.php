@extends('app')

@section('content')
	<div id="wrapper">

		<!-- Navigation -->
		@include('menu')

		<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Users</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading text-right">
                            <a href="{{ url('/users/create') }}" class="btn btn-primary text-right">
                                <i class="fa fa-plus-square"></i>
                                Add new
                            </a>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper table-responsive">
                                <table class="table table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Profile</th>
                                            <th>Status</th>
                                            <th>Options</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	@if (isset($users) AND count($users) > 0)
                                    		@foreach ($users as $user)
												<tr class="odd gradeX">
	                                            <td>{{$user->name}}</td>
	                                            <td>{{$user->email}}</td>
	                                            <td>{{$user->profile}}</td>
	                                            <td class="center">
                                                    @if ($user->state == 1)
                                                        <span class="btn btn-success btn-circle" data-toggle="tooltip" data-placement="top" title="Active">
                                                            <i class="fa fa-info"></i>
                                                        </span>
                                                    @else
                                                        <span class="btn btn-warning btn-circle" data-toggle="tooltip" data-placement="top" title="Inactive">
                                                            <i class="fa fa-info"></i>
                                                        </span>
                                                    @endif
                                                </td>
	                                            <td class="center tdOptions">
                                                    <div class="btn-group">
                                                        <button type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown">
                                                            <i class="fa fa-gear"></i>  <span class="caret"></span>
                                                        </button>
                                                        <ul class="dropdown-menu" role="menu">
                                                            <li>
                                                                <a href="{{ url('/users/'.$user->id.'/edit') }}">
                                                                    <i class="fa fa-edit"></i>
                                                                    Edit</a>
                                                            </li>
                                                            <li>
                                                                {!! Form::open(array('method' => 'DELETE','route' => ['users.destroy',$user->id])) !!}

                                                                <button type="submit" class="fa fa-trash">
                                                                    Delete
                                                                </button>

                                                                {!! Form::close() !!}
                                                            </li>
                                                        </ul>
                                                    </div>


                                                   <!-- <a href="{{ url('/users/'.$user->id.'/edit') }}">
	                                            		<i class="fa fa-edit"></i>
	                                            	</a>

                                                    {!! Form::open(array('method' => 'DELETE','route' => ['users.destroy',$user->id])) !!}

                                                        <button type="submit" class="fa fa-trash"></button>

                                                    {!! Form::close() !!}

-->
	                                            </td>                                            
	                                        </tr>
											@endforeach
										@else
											<tr class="" colspan="5">
                                             	<td>No results </td>
                                        	</tr>
                                    	@endif
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
               
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
       
		<!-- /#page-wrapper -->

	</div>
	<!-- /#wrapper -->
@endsection