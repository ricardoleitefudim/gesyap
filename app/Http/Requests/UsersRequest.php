<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class UsersRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		//adicionar regras para perfil
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{



		return [
			'name' => 'required|min:3',
			'email' => 'required|email',
			'password' => 'required',
			'state' => 'required',
			'profile' => 'required'
		];

		/*
'name' => 'required|max:255',
			'email' => 'required|email|max:255|unique:users',
			'password' => 'required|confirmed|min:6',
		*/
	}

}
