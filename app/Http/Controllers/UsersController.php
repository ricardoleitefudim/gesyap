<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Http\Requests\UsersRequest;
use App\User;

class UsersController extends Controller {



	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
		$users = User::all();

		return view('users.index',compact('users'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
		return view('users.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(UsersRequest $request)
	{
		//
		User::create($request->all());

        return redirect('users');

	}

	/**
	 * Display the specified resource.
	 *
	 * @param  User $user
	 * @return Response
	 */
	public function show(User $user)
	{
		//
		//return view('users.edit',compact('user'));

	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  User $user
	 * @return Response
	 */
	public function edit(User $user)
	{
		//
		return view('users.edit',compact('user'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param  User $user
     * @param UsersRequest $request
     * @return Response
     */
	public function update(User $user, UsersRequest $request)
	{
		//
        $user->update($request->all());

        return redirect('users');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  User $user
	 * @return Response
	 */
	public function destroy(User $user)
	{
		//

        $user->delete();

        return redirect('users');
	}

}
