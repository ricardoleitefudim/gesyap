<?php namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests;
use App\Http\Requests\ClientsRequest;


class ClientsController extends Controller {


    /**
     * Create a new controller instance.
     *
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
        $clients = Client::all();

        return view('clients.index',compact('clients'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
        return view('clients.create');
	}

    /**
     * Store a newly created resource in storage.
     *
     * @param ClientsRequest $request
     * @return Response
     */
	public function store(ClientsRequest $request)
	{
		//
        Client::create($request->all());

        return redirect('clients');
	}

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @return Response
     */
	public function show(Client $client)
	{
        dd($client);
		//
        return view('clients.edit',compact('client'));
	}

    /**
     * Show the form for editing the specified resource.
     *
     * @param Client $client
     * @return Response
     */
	public function edit(Client $client)
	{
        //dd($client);
		//
        return view('clients.edit',compact('client'));
	}

    /**
     * Update the specified resource in storage.
     *
     * @param Client $client
     * @param ClientsRequest $request
     * @return Response
     */
    public function update(Client $client, ClientsRequest $request)
    {
        //
        $client->update($request->all());

        return redirect('clients');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Client $client
     * @return Response
     */
	public function destroy(Client $client)
	{
		//
        $client->delete();

        return redirect('clients');
	}

}
